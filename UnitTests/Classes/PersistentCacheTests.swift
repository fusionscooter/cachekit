//
//  PersistentCacheTests.swift
//  UnitTests
//
//  Created by Brett Hamlin on 2/21/20.
//

import XCTest
import CacheKit

class PersistentCacheTests: XCTestCase {
    
    enum TestData {
        static let cacheKey = "testCacheKey"
        static let cacheKeyExpiredTime: TimeInterval = 1
        static let cacheKeyExpiredNotificationName = Notification.Name("testCacheKeyExpired")
        static let cacheTestDataKey = "Brett"
        static let cacheTestDataValue = "Hamlin"
        static let cache = [TestData.cacheTestDataKey : TestData.cacheTestDataValue]
        static let cacheTestMetaDataKey = "MimeType"
        static let cacheTestMetaDataValue = "JSON"
        static let metaData = [TestData.cacheTestMetaDataKey : TestData.cacheTestMetaDataValue]
        static let customCacheConfigExpiration: TimeInterval = 1
    }
    
    let data = try! JSONSerialization.data(withJSONObject: TestData.cache, options: [])
    
    override func setUp() {
        CacheController.shared.resetAllSettings()
        super.setUp()
    }

    func testPersistentDataCache() {
        let config = CacheUnitConfig.persistent(cacheKey: TestData.cacheKey, dataType: .data(data: data))
        CacheController.shared.createCacheUnit(with: config)
        
        let fetchedCache: PersistentCacheUnit? = CacheController.shared.cacheUnit(for: TestData.cacheKey)
        guard let data = fetchedCache?.data,
            let jsonObject = try? JSONSerialization.jsonObject(with: data, options: []) as? Dictionary<String, String>,
            let firstValue = jsonObject[TestData.cacheTestDataKey] else {
                XCTFail("Problem with data.")
                return
        }
        
        XCTAssertEqual(firstValue, TestData.cacheTestDataValue)
    }
    
    func testPersistentFileCache() {
        guard let filePath = Bundle(for: type(of: self)).url(forResource: "PeopleNames", withExtension: "json") else {
            XCTFail("Failed to get JSON file path.")
            return
        }
        
        let config = CacheUnitConfig.persistent(cacheKey: TestData.cacheKey, dataType: .file(file: filePath))
        CacheController.shared.createCacheUnit(with: config)
        
        let fetchedCache: PersistentCacheUnit? = CacheController.shared.cacheUnit(for: TestData.cacheKey)
        guard let data = fetchedCache?.data,
            let jsonObject = try? JSONSerialization.jsonObject(with: data, options: []) as? Dictionary<String, String>,
            let firstValue = jsonObject[TestData.cacheTestDataKey] else {
                XCTFail("Problem with data.")
                return
        }
        
        XCTAssertEqual(firstValue, TestData.cacheTestDataValue)
    }
    
    func testPersistentCacheExpired() {
        let config = CacheUnitConfig.persistent(cacheKey: TestData.cacheKey, expiration: TestData.cacheKeyExpiredTime, dataType: .data(data: data))
        CacheController.shared.createCacheUnit(with: config)
        
        Timer.scheduledTimer(withTimeInterval: TestData.cacheKeyExpiredTime + 1, repeats: false) { (timer) in
             NotificationCenter.default.post(name: TestData.cacheKeyExpiredNotificationName, object: self)
         }
        
        let expectation = XCTNSNotificationExpectation(name: TestData.cacheKeyExpiredNotificationName)
        wait(for: [expectation], timeout: TestData.cacheKeyExpiredTime + 5)
        
        let fetchedCache: PersistentCacheUnit? = CacheController.shared.cacheUnit(for: TestData.cacheKey)
        XCTAssertNil(fetchedCache, "Cache still exists")
    }
    
    func testPersistentCacheMetaData() {
        let config = CacheUnitConfig.persistent(cacheKey: TestData.cacheKey, dataType: .data(data: data), metaData: TestData.metaData)
        CacheController.shared.createCacheUnit(with: config)
        
        let fetchedCache: PersistentCacheUnit? = CacheController.shared.cacheUnit(for: TestData.cacheKey)
        guard let jsonObject = fetchedCache?.metaData as? Dictionary<String, String>,
            let firstValue = jsonObject[TestData.cacheTestMetaDataKey] else {
                XCTFail("Problem with data.")
                return
        }
        
        XCTAssertEqual(firstValue, TestData.cacheTestMetaDataValue)
    }
    
    func testPersistentCacheConfigExpiration() {
        CacheController.shared.setDefault(config: CacheDefaultConfig.persistent(expiration: TestData.customCacheConfigExpiration), with: .persistentType)
        
        let config = CacheUnitConfig.persistent(cacheKey: TestData.cacheKey, dataType: .data(data: data))
        CacheController.shared.createCacheUnit(with: config)
        
        Timer.scheduledTimer(withTimeInterval: TestData.customCacheConfigExpiration + 1, repeats: false) { (timer) in
             NotificationCenter.default.post(name: TestData.cacheKeyExpiredNotificationName, object: self)
         }
        
        let expectation = XCTNSNotificationExpectation(name: TestData.cacheKeyExpiredNotificationName)
        wait(for: [expectation], timeout: TestData.customCacheConfigExpiration + 5)
        
        let fetchedCache: PersistentCacheUnit? = CacheController.shared.cacheUnit(for: TestData.cacheKey)
        XCTAssertNil(fetchedCache, "Cache still exists")
    }
}
