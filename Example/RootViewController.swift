//
//  RootViewController.swift
//  CacheKit Example
//
//  Created by Brett Hamlin on 2/18/20.
//  Copyright © 2020 CocoaPods. All rights reserved.
//

import UIKit
import CacheKit

class RootViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        cacheIt()
    }
    
    private func cacheIt() {
        // Lets store a dictionary in memory cache using the default parameters
        TransientCache["MyName"] = ["Brett" : "Hamlin"]
                
        // Fetch MyName from in memory cache
        let myName: [String:String]? = TransientCache["MyName"]
        print("Value of in memory MyName = \(myName ?? ["Some thing went wrong":"Assert()!?!?! Fail()!! doesNotCompute(@)!???"])")

        
        // Now lets store an array to disk
        PersistentCache["ev0lutionOfApp"] = ["NextOS", "Objective-C", "Swift", "???"]
        
        // Fetch ev0lutionOfApp from in disk cache
        let evo: [String]? = PersistentCache["ev0lutionOfApp"]
        print("Value of persistent cache ev0lutionOfApp = \(evo ?? ["Some thing went wrong Assert()!?!?! Fail()!! doesNotCompute(@)!???"])")
        
        
        // And finally lets store an image to disk with a custom expiration time of 60 seconds
        if #available(iOS 13.0, *) {
            var persistentCache = PersistentCache(expiration: 60)
            let image = UIImage(systemName: "multiply.circle.fill")
            persistentCache["image"] = image
            
            let loadedImage: UIImage? = persistentCache["image"]
            print("Value of persistent cache image = \(loadedImage?.description ?? "Some thing went wrong Assert()!?!?! Fail()!! doesNotCompute(@)!???")")
        }
    }
}
 
