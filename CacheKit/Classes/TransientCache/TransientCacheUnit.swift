//
//  TransientCacheUnit.swift
//  CacheKit
//
//  Created by Brett Hamlin on 2/17/20.
//

import Foundation

public final class TransientCacheUnit: CacheUnit {
    public static var cacheType: CacheType = .transientType    
    public let cacheKey: CacheKey
    public let data: Data
    public let metaData: [String:Any]?
    public var expired: Bool {
        return Date().timeIntervalSinceNow > self.expiration.timeIntervalSinceNow
    }
    private var cacheManager: TransientCacheManager
    private let expiration: Date
    
    init(cacheKey: CacheKey, cacheManager: TransientCacheManager, expiration: Date, data: Data, metaData: [String:Any]?) {
        self.cacheKey = cacheKey
        self.cacheManager = cacheManager
        self.expiration = expiration
        self.data = data
        self.metaData = metaData
        
        Timer.scheduledTimer(withTimeInterval: self.expiration.timeIntervalSinceNow - Date().timeIntervalSinceNow, repeats: false) { [weak self] _ in
            guard let self = self else { return }
            print("*** Expiring ***\n\(self)")
            self.cacheManager.removeCache(for: cacheKey)
        }
    }
}

extension TransientCacheUnit: CustomStringConvertible {
    public var description: String {
        return  """
                CacheKey: \(cacheKey)
                CacheType: transient
                Expired: \(expired)
                """
    }
}
