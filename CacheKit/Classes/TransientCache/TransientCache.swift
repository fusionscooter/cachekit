//
//  TransientCache.swift
//  CacheKit
//
//  Created by Brett Hamlin on 2/22/20.
//

import Foundation

public struct TransientCache {
    let expiration: TimeInterval
    
    public init(expiration: TimeInterval) {
        self.expiration = expiration
    }

    public static subscript<T: Any>(key: CacheKey) -> T? {
        get {
            let cacheUnit: TransientCacheUnit? = CacheController.shared.cacheUnit(for: key)
            guard let data = cacheUnit?.data else { return nil }
            return NSKeyedUnarchiver.unarchiveObject(with: data) as? T
        }
        set {
            guard let data = newValue else {
                   return
               }
            let archivedData = NSKeyedArchiver.archivedData(withRootObject: data)
            let config = CacheUnitConfig.transient(cacheKey: key, data: archivedData)
            CacheController.shared.createCacheUnit(with: config)
        }
    }
    
    public subscript<T: Any>(key: CacheKey) -> T? {
        get {
           return type(of: self)[key]
        }
        set {
            guard let data = newValue else {
                   return
               }
            let archivedData = NSKeyedArchiver.archivedData(withRootObject: data)
            let config = CacheUnitConfig.transient(cacheKey: key, expiration: expiration, data: archivedData)
            CacheController.shared.createCacheUnit(with: config)
        }
    }
}
