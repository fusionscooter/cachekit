//
//  PersistentCache.swift
//  CacheKit
//
//  Created by Brett Hamlin on 2/22/20.
//

import Foundation

public struct PersistentCache {
    let expiration: TimeInterval
    
    public init(expiration: TimeInterval) {
        self.expiration = expiration
    }

    public static subscript<T: Any>(key: CacheKey) -> T? {
        get {
            let cacheUnit: PersistentCacheUnit? = CacheController.shared.cacheUnit(for: key)
            guard let data = cacheUnit?.data else { return nil }
            return NSKeyedUnarchiver.unarchiveObject(with: data) as? T
        }
        set {
            guard let data = newValue else {
                return
            }
            let archivedData = NSKeyedArchiver.archivedData(withRootObject: data)
            let config = CacheUnitConfig.persistent(cacheKey: key, dataType: .data(data: archivedData))
            CacheController.shared.createCacheUnit(with: config)
        }
    }
    
    public subscript<T: Any>(key: CacheKey) -> T? {
        get {
           return type(of: self)[key]
        }
        set {
            guard let data = newValue else {
                return
            }
            let archivedData = NSKeyedArchiver.archivedData(withRootObject: data)
            let config = CacheUnitConfig.persistent(cacheKey: key, expiration: expiration, dataType: .data(data: archivedData))
            CacheController.shared.createCacheUnit(with: config)
        }
    }    
}
