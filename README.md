# CacheKit

**CacheKit** makes it easy to add caching capabilities to your iOS app! 

## Requirements

- Swift 5.0
- iOS 10.0+


## Installation
### Cocoapods

[CocoaPods](http://cocoapods.org) is a dependency manager for Cocoa projects.

To integrate **CacheKit** into your Xcode project using CocoaPods, specify it in your `Podfile`:

```rubygi
source 'https://github.com/CocoaPods/Specs.git'
platform :ios, '10.0'
use_frameworks!

target '<Your Target Name>' do
    pod 'CacheKit', '>= 1.0'
end
```

Then, run the following command:

```bash
$ pod install
```

## Usage default parameters

1. Import CacheKit Module

```swift
import CacheKit
```

2. Store data to disk using the static class function referencing the data by the key "names" using the default storage parameters.
```swift
PersistentCache["names"] = ["Brett" : "Hamlin"]
```

3. Retrieve data from disk using the static class function referencing it by the key "names".
```swift
let myNames: [String:String]? = PersistentCache["names"]
```

## Usage custom parameters

1. Import CacheKit Module

```swift
import CacheKit
```

2. Create a custom cache type with your own parameters.
```swift
var cache = PersistentCache(expiration: 30)
```

3. Store data in memory referencing the data by the key "names" using custom storage parameters.
```swift
cache["names"] = ["Brett", "Art", "Dan"]
```

3. Retrieve data from memory referencing it by the key "names".
```swift
let myNames: [String]? = cache["names"]
```
## Details, details, details
**Caching Types:** CacheKit supports Transient (to memory) and Persistent (to disk) caching.  You can reference the static class subscript methods using default parameters or create your own instance of the class to pass in your own default parameters.  The two classes are: PersistentCache and TransientCache respectively.



## License

**CacheKit** is available under the MIT license. 
